<?php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config.php';
use Game\CharacterFactory;

class Battle
{
    private \Game\Hero $hero;
    private \Game\Beast $beast;
    private int $round;
    private int $maxRounds;

    public function __construct($maxRounds) {
        $this->hero = CharacterFactory::createCharacter(HERO_NAME , rand(HERO_MIN_HP, HERO_MAX_HP),
                                                        rand(HERO_MIN_STRENGTH, HERO_MAX_STRENGTH), rand(HERO_MIN_DEF, HERO_MAX_DEF),
                                                        rand(HERO_MIN_SPEED, HERO_MAX_SPEED), rand(HERO_MIN_LUCK, HERO_MAX_LUCK));
        $this->hero->addSkill('Rapid strike', 10);
        $this->hero->addSkill('Magic shield',20);

        $this->beast = CharacterFactory::createBeast(BEAST_NAME, rand(BEAST_MIN_HP, BEAST_MAX_HP),
                                                    rand(BEAST_MIN_STRENGTH, BEAST_MAX_STRENGTH), rand(BEAST_MIN_DEF, BEAST_MAX_DEF),
                                                    rand(BEAST_MIN_SPEED, BEAST_MAX_SPEED), rand(BEAST_MIN_LUCK, BEAST_MAX_LUCK));
        $this->maxRounds = $maxRounds;
        $this->round = 1;
    }

    private function changeRoles($hero, $beast){
        $this->beast->setCurrentTurn(!$beast->getCurrentTurn());
        $this->hero->setCurrentTurn(!$hero->getCurrentTurn());
    }

    private function attackBeast(){
        $damage = $this->hero->getStrength() - $this->beast->getDefence();

        if($this->beast->dodgeAttack()){
            echo $this->beast->getName()." dodges the attack.\n";
            $damage = 0;
        }

        $this->beast->setHealth($this->beast->getHealth() - $damage);
        echo $this->beast->getName()." took ".$damage." damage, remaining with ".$this->beast->getHealth()." HP.\n";
    }

    private function attackHero(){
        $damage = $this->beast->getStrength() - $this->hero->getDefence();

        if($this->hero->dodgeAttack()){
            echo $this->hero->getName()." dodges the attack.\n";
            $damage = 0;
        } else if(!$this->hero->getSkills()[1]->useSpell()){
            echo $this->hero->getName()." casts ".$this->hero->getSkills()[1]->getName().".\n";
            $damage /= 2;
        }

        $this->hero->setHealth($this->hero->getHealth() - $damage);
        echo $this->hero->getName()." took ".$damage." damage, remaining with ".$this->hero->getHealth()." HP.\n";
    }

    public function startBattle()
    {
        if($this->hero->getSpeed() > $this->beast->getSpeed()) {
            $this->hero->setCurrentTurn(true);
        } elseif($this->beast->getSpeed() > $this->hero->getSpeed()) {
            $this->hero->setCurrentTurn(true);
        } else {
            $this->hero->getLuck() > $this->beast->getLuck() ? $this->hero->setCurrentTurn(true) : $this->beast->setCurrentTurn(true);
        }
        echo $this->hero->getName()."' stats: ". $this->hero->getHealth()." HP, ".$this->hero->getStrength()." strength, ".$this->hero->getDefence()." defence, ".$this->hero->getSpeed()." speed, ".$this->hero->getLuck()." luck\n";
        echo $this->beast->getName()."'s stats: ". $this->beast->getHealth()." HP, ".$this->beast->getStrength()." strength, ".$this->beast->getDefence()." defence, ".$this->beast->getSpeed()." speed, ".$this->beast->getLuck()." luck\n";

        while($this->round <= $this->maxRounds)
        {
            echo "----- Round $this->round -----\n";
            if($this->hero->getCurrentTurn() == true)
            {
                echo "It's ".$this->hero->getName()."' turn to attack.\n";
                $this->attackBeast();

                if($this->hero->getSkills()[0]->useSpell() && $this->beast->getHealth() > 0){
                    echo $this->hero->getName()." casts ".$this->hero->getSkills()[0]->getName().".\n";
                    $this->attackBeast();
                }

            } else{
                echo "It's ".$this->beast->getName()."'s turn to attack.\n";

                $this->attackHero();
            }

            if($this->hero->getHealth() <= 0){
                echo $this->beast->getName()." wins the fight! HP left: ".$this->beast->getHealth();
                return;
            }
            elseif($this->beast->getHealth() <= 0){
                echo $this->hero->getName()." wins the fight! HP left: ".$this->hero->getHealth();
                return;
            }

            $this->changeRoles($this->hero, $this->beast);
            $this->round += 1;
        }

        echo "----- Fight ended with no winner -----\n";
        echo $this->hero->getName()."' remaining health: ".$this->hero->getHealth()." HP\n";
        echo $this->beast->getName()."'s remaining health: ".$this->beast->getHealth()." HP\n";
    }
}

$battle = new Battle(20);
$battle->startBattle();