<?php
define('HERO_NAME', 'Orderus');
define('HERO_MIN_HP', 70);
define('HERO_MAX_HP', 100);
define('HERO_MIN_STRENGTH', 70);
define('HERO_MAX_STRENGTH', 80);
define('HERO_MIN_DEF', 45);
define('HERO_MAX_DEF', 55);
define('HERO_MIN_SPEED', 40);
define('HERO_MAX_SPEED', 50);
define('HERO_MIN_LUCK', 10);
define('HERO_MAX_LUCK', 30);

define('BEAST_NAME', 'Wild beast');
define('BEAST_MIN_HP', 60);
define('BEAST_MAX_HP', 90);
define('BEAST_MIN_STRENGTH', 60);
define('BEAST_MAX_STRENGTH', 90);
define('BEAST_MIN_DEF', 40);
define('BEAST_MAX_DEF', 60);
define('BEAST_MIN_SPEED', 40);
define('BEAST_MAX_SPEED', 60);
define('BEAST_MIN_LUCK', 25);
define('BEAST_MAX_LUCK', 40);