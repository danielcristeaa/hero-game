<?php


namespace Game;

class CharacterFactory
{
    public static function createCharacter($name, $health, $strength, $defence, $speed, $luck)
    {
        return new Hero($name, $health, $strength, $defence, $speed, $luck, false);
    }

    public static function createBeast($name, $health, $strength, $defence, $speed, $luck)
    {
        return new Beast($name, $health, $strength, $defence, $speed, $luck, false);
    }
}