<?php


namespace Game;


class Beast
{
    private string $name;
    private int $health;
    private int $strength;
    private int $defence;
    private int $speed;
    private int $luck;
    private bool $currentTurn;

    /**
     * Beast constructor.
     * @param $name
     * @param $health
     * @param $strength
     * @param $defence
     * @param $speed
     * @param $luck
     * @param $currentTurn
     */
    public function __construct($name, $health, $strength, $defence, $speed, $luck, $currentTurn)
    {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
        $this->currentTurn = $currentTurn;
    }

    public function dodgeAttack(){
        $random = rand(0 , 100);
        return $random < $this->luck;
    }

    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param int $health
     * @return Beast
     */
    public function setHealth($health)
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @return mixed
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @return mixed
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCurrentTurn()
    {
        return $this->currentTurn;
    }

    /**
     * @param boolean $currentTurn
     * @return Beast
     */
    public function setCurrentTurn($currentTurn)
    {
        $this->currentTurn = $currentTurn;
        return $this;
    }


}