<?php


namespace Game;


class Skill
{
    private string $name;
    private int $chance;

    /**
     * Skill constructor.
     * @param $name
     * @param $chance
     */
    public function __construct($name, $chance)
    {
        $this->name = $name;
        $this->chance = $chance;
    }

    public function useSpell()
    {
        $random = rand(0 , 100);
        return $random < $this->chance;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}