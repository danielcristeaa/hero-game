<?php


namespace Game;


class Hero
{
    private $name;
    private $health;
    private $strength;
    private $defence;
    private $speed;
    private $luck;
    private $skills;
    private $currentTurn;

    /**
     * Hero constructor.
     * @param $name
     * @param $health
     * @param $strength
     * @param $defence
     * @param $speed
     * @param $luck
     * @param $currentTurn
     */
    public function __construct($name, $health, $strength, $defence, $speed, $luck, $currentTurn)
    {
        $this->name = $name;
        $this->health = $health;
        $this->strength = $strength;
        $this->defence = $defence;
        $this->speed = $speed;
        $this->luck = $luck;
        $this->skills = [];
        $this->currentTurn = $currentTurn;
    }

    public function dodgeAttack(){
        $random = rand(0 , 100);
        return $random < $this->luck;
    }

    public function addSkill($name, $chance)
    {
        $skill = new Skill($name, $chance);
        array_push($this->skills, $skill);
    }

    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param mixed $health
     * @return Hero
     */
    public function setHealth($health)
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @return mixed
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @return mixed
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCurrentTurn()
    {
        return $this->currentTurn;
    }

    /**
     * @param mixed $currentTurn
     * @return Hero
     */
    public function setCurrentTurn($currentTurn)
    {
        $this->currentTurn = $currentTurn;
        return $this;
    }


}