<?php

require __DIR__.'/../config.php';
use PHPUnit\Framework\TestCase;
use Game\CharacterFactory;
use Game\Beast;

class BeastTest extends TestCase
{

    public function testSetHealthInvalid()
    {
        $beast = CharacterFactory::createBeast(BEAST_NAME, rand(BEAST_MIN_HP, BEAST_MAX_HP),
            rand(BEAST_MIN_STRENGTH, BEAST_MAX_STRENGTH), rand(BEAST_MIN_DEF, BEAST_MAX_DEF),
            rand(BEAST_MIN_SPEED, BEAST_MAX_SPEED), rand(BEAST_MIN_LUCK, BEAST_MAX_LUCK));
        $this->expectException(TypeError::class);
        $beast->setHealth('string');
    }

    public function testConstructInvalid()
    {
        $this->expectException(TypeError::class);
        $beast = CharacterFactory::createBeast(BEAST_NAME, 'string',
            rand(BEAST_MIN_STRENGTH, BEAST_MAX_STRENGTH), rand(BEAST_MIN_DEF, BEAST_MAX_DEF),
            rand(BEAST_MIN_SPEED, BEAST_MAX_SPEED), rand(BEAST_MIN_LUCK, BEAST_MAX_LUCK));
    }
}
