<?php

require __DIR__.'/../config.php';
use Game\Hero;
use Game\CharacterFactory;
use PHPUnit\Framework\TestCase;

class HeroTest extends TestCase
{
    protected Hero $hero;
    protected function setUp(): void
    {
        $this->hero = CharacterFactory::createCharacter(HERO_NAME , rand(HERO_MIN_HP, HERO_MAX_HP),
            rand(HERO_MIN_STRENGTH, HERO_MAX_STRENGTH), rand(HERO_MIN_DEF, HERO_MAX_DEF),
            rand(HERO_MIN_SPEED, HERO_MAX_SPEED), rand(HERO_MIN_LUCK, HERO_MAX_LUCK));
    }

    public function testSetCurrentTurn()
    {
        $this->hero->setCurrentTurn(true);
        $this->assertTrue($this->hero->getCurrentTurn());
    }

    public function testAddSkill()
    {
        $this->hero->addSkill('Test skill', 20);

        $this->assertEquals('Test skill', $this->hero->getSkills()[0]->getName());
    }

    public function testSetHealth()
    {
        $newHealth = 80;
        $this->hero->setHealth($newHealth);
        $this->assertEquals($newHealth, $this->hero->getHealth());
    }
}
